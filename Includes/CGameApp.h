//-----------------------------------------------------------------------------
// File: CGameApp.h
//
// Desc: Game Application class, this is the central hub for all app processing
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

#ifndef _CGAMEAPP_H_
#define _CGAMEAPP_H_

//-----------------------------------------------------------------------------
// CGameApp Specific Includes
//-----------------------------------------------------------------------------
#include "Main.h"
#include "CTimer.h"
#include "CPlayer.h"
#include "BackBuffer.h"
#include "ImageFile.h"
#include "Enemy.h"

#include <iostream>
#include <string>
#include <fstream>

#define IDCONTINUEGAME 1

//-----------------------------------------------------------------------------
// Forward Declarations
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Main Class Declarations
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Name : CGameApp (Class)
// Desc : Central game engine, initialises the game and handles core processes.
//-----------------------------------------------------------------------------
class CGameApp
{
public:
	//-------------------------------------------------------------------------
	// Constructors & Destructors for This Class.
	//-------------------------------------------------------------------------
			 CGameApp();
	virtual ~CGameApp();

	//-------------------------------------------------------------------------
	// Public Functions for This Class
	//-------------------------------------------------------------------------
	LRESULT	 DisplayWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam );
	bool		InitInstance( LPCTSTR lpCmdLine, int iCmdShow );
	int		 BeginGame( );
	bool		ShutDown( );
	int RestartGame(LPCTSTR lpCmdLine, int iCmdShow);
	void Save();
	void SaveSinglePlayer();
	void SaveMultiPlayer();
	void Load();


private:
	//-------------------------------------------------------------------------
	// Private Functions for This Class
	//-------------------------------------------------------------------------
	bool		BuildObjects	  ( );
	bool BuildObjectsSinglePlayer();
	bool BuildObjectsMultiPlayer();
	void		ReleaseObjects	( );
	int			FrameAdvance();
	bool		CreateDisplay	 ( );
	void SelectGameMode();
	void		ChangeDevice	  ( );
	void		SetupGameState	( );
	void		AnimateObjects	( );
	void CheckCollisionsSinglePlayer();
	void CheckCollisionsMultiPlayer();
	void CheckCollisions();
	void		DrawObjects	   ( );
	void		ProcessInput	  ( );
	void LoadSinglePlayer();
	void LoadMultiPlayer();
	
	string		save_file = "data/saveFile.txt";
	//CPlayer* m_pOponentPlayer;

	//-------------------------------------------------------------------------
	// Private Static Functions For This Class
	//-------------------------------------------------------------------------
	static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam);

	//-------------------------------------------------------------------------
	// Private Variables For This Class
	//-------------------------------------------------------------------------
	CTimer				  m_Timer;			// Game timer
	ULONG				   m_LastFrameRate;	// Used for making sure we update only when fps changes.
	
	HWND					m_hWnd;			 // Main window HWND
	HICON				   m_hIcon;			// Window Icon
	HMENU				   m_hMenu;			// Window Menu
	
	bool					m_bActive;		  // Is the application active ?

	ULONG				   m_nViewX;		   // X Position of render viewport
	ULONG				   m_nViewY;		   // Y Position of render viewport
	ULONG				   m_nViewWidth;	   // Width of render viewport
	ULONG				   m_nViewHeight;	  // Height of render viewport

	POINT				   m_OldCursorPos;	 // Old cursor position for tracking
	HINSTANCE				m_hInstance;

	CImageFile				m_imgBackground;

	BackBuffer*				m_pBBuffer;
	vector<CPlayer*>		m_pPlayers;
	vector<Enemy*>			m_pEnemies;

	ATOM					atom = 0;
	double					scrolling_backgroung = 0;
	double					SCROLLING_SPEED = 0.5;
	int						NEW_ENEMY_CHANCHE = 100000;
	string					CPLAYER_INPUT_FILE = "data\\bullet.bmp";
	string					ENEMY_INPUT_FILE = "data\\bullet.bmp";
	string					BACKGROUND_FILE = "data\\mw.bmp";
	int						new_enemy;
	int						SINGLE_PLAYER = 1;
};

#endif // _CGAMEAPP_H_