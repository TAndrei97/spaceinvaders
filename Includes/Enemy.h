//-----------------------------------------------------------------------------
// File: CPlayer.cpp
//
// Desc: This file stores the player object class. This class performs tasks
//	   such as player movement, some minor physics as well as rendering.
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

#ifndef _ENEMY_H_
#define _ENEMY_H_

#define MAX_SPEED 100
#define MAX_MOVE_SPEED 100
//-----------------------------------------------------------------------------
// CPlayer Specific Includes
//-----------------------------------------------------------------------------
#include "Main.h"
#include "Sprite.h"
#include <iostream>
#include <vector>

using namespace std;

//-----------------------------------------------------------------------------
// Main Class Definitions
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Name : CPlayer (Class)
// Desc : Player class handles all player manipulation, update and management.
//-----------------------------------------------------------------------------
class Enemy
{
public:
	//-------------------------------------------------------------------------
	// Enumerators
	//-------------------------------------------------------------------------
	enum DIRECTION
	{
		DIR_FORWARD = 1,
		DIR_BACKWARD = 2,
		DIR_LEFT = 4,
		DIR_RIGHT = 8,
	};

	enum ESpeedStates
	{
		SPEED_START,
		SPEED_STOP
	};

	//-------------------------------------------------------------------------
	// Constructors & Destructors for This Class.
	//-------------------------------------------------------------------------
	Enemy(const BackBuffer *pBackBuffer, const char *aBulletFileName);
	virtual ~Enemy();

	//-------------------------------------------------------------------------
	// Public Functions for This Class.
	//-------------------------------------------------------------------------
	void					Update(float dt);
	void					Draw();
	void					Move();
	Vec2&					Position();
	Vec2&					Velocity();

	void					Shoot();

	void					Explode();
	bool					AdvanceExplosion();

	vector<Sprite*>			getBullets() const { return m_bulletSpriteVector; }
	bool					checkCollision(vector<Sprite*> bullets);
	void					deleteBullets();
	string					toString();
	Sprite*					CreateBulletSprite(int angle);	
	friend std::istream & operator >> (std::istream & in, Enemy* enemy);
private:
	//-------------------------------------------------------------------------
	// Private Variables for This Class.
	//-------------------------------------------------------------------------
	Sprite*					m_eSprite;
	ESpeedStates			m_eSpeedState;
	float					m_fTimer;

	vector<Sprite*>			m_bulletSpriteVector;
	const BackBuffer*		backBuffer;

	bool					m_bExplosion;
	AnimatedSprite*			m_pExplosionSprite;
	int						m_iExplosionFrame;

	Vec2					center;

	char					bulletFileName[MAX_PATH];

	bool					isInsideTheFrame() const;
	int						angle;
	//int						MAX_MOVE_SPEED = 150;
};

#endif // _ENEMY_H_