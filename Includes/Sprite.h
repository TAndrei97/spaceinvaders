// Sprite.h
// By Frank Luna
// August 24, 2004.
#ifndef SPRITE_H
#define SPRITE_H

#include "main.h"
#include "Vec2.h"
#include "BackBuffer.h"
#include <memory>
#include <vector>
#include <string>
//#include "gdiplus.h"
#define MAX_SPEED 100
using namespace std;
//using namespace Gdiplus;

#define ANGLE_ROTATION 3

class Sprite
{
public:
	Sprite(int imageID, int maskID);
	Sprite(const char *szImageFile, const char *szMaskFile);
	Sprite(const char *szImageFile, COLORREF crTransparentColor, int rotation_angle = 0);
	void CreateRotationFrames();

	virtual ~Sprite();

	int width() const { return mImageBM.bmWidth; }
	int height() const { return mImageBM.bmHeight; }

	int buffer_width() { return mpBackBuffer->width(); }
	int buffer_height() { return mpBackBuffer->height(); }

	void update(float dt);

	void setBackBuffer(const BackBuffer *pBackBuffer);
	virtual void draw();

	bool isCollision(const Sprite* other);
	int GetPixelToCheck(const Sprite* other);

	void set_apex(Vec2 apex) { this->apex = apex; }
	Vec2 get_apex() const { return apex; }

	void rotate(int angle) {}
	int get_angle() { return angle; }
	void set_angle(int angle_rotation) { angle = angle_rotation; }
	
	void Rotate(int angle);
	void UpdatePixelBuffer();
	bool isApexInsideSprite(const Sprite* other);
	bool isPointInsideSprite(const Vec2 point);
	void Rotate(float radians, const char* szImageFile);
	void	computeVelocity();
	void	computeCenter();
	string	toString();
	friend std::istream & operator >> (std::istream & in, Sprite* sprite);


public:
	// Keep these public because they need to be
	// modified externally frequently.
	Vec2 mPosition;
	Vec2 mVelocity;

private:
	// Make copy constructor and assignment operator private
	// so client cannot copy Sprites. We do this because
	// this class is not designed to be copied because it
	// is not efficient--copying bitmaps is slow (lots of memory).
	Sprite(const Sprite& rhs);
	Sprite& operator=(const Sprite& rhs);

protected:

	int angle;

	Vec2 apex;

	HBITMAP auxmhImage;
	HBITMAP mhImage;
	HBITMAP mhMask;
	BITMAP mImageBM;
	BITMAP mMaskBM;

	HDC mhSpriteDC;
	const BackBuffer *mpBackBuffer;

	COLORREF mcTransparentColor;
	void drawTransparent();
	void TransformAndDraw(INT iTransform, HWND hWnd);
	HBITMAP GetRotatedBitmapNT(HBITMAP hBitmap, float radians, COLORREF clrBack);
	void Pixel();

	void Rotate1(float radians, const char* szImageFile);
	void drawMask();
	vector<unsigned char> ToPixels(HBITMAP BitmapHandle, int& width, int& height);
	bool isSamePixelColor(const COLORREF first_pixel, const COLORREF second_pixel);
	
	//HBITMAP rotated_images[360 / ANGLE_ROTATION];
	vector<HBITMAP> rotated_images;
	vector<COLORREF> image_pixels;

	
};

// AnimatedSprite
// by Mihai Popescu
// April 5, 2008
class AnimatedSprite : public Sprite
{
public:
	//NOTE: The animation is on a single row.
	AnimatedSprite(const char *szImageFile, const char *szMaskFile, const RECT& rcFirstFrame, int iFrameCount);
	virtual ~AnimatedSprite() { }

public:
	void SetFrame(int iIndex);
	int GetFrameCount() { return miFrameCount; }

	virtual void draw();

protected:
	POINT mptFrameStartCrop;// first point of the frame (upper-left corner)
	POINT mptFrameCrop;		// crop point of frame
	int miFrameWidth;		// width
	int miFrameHeight;		// height
	int miFrameCount;		// number of frames
};



#endif // SPRITE_H

