
#include "Sprite.h"
#include <iostream>
#include <vector>

extern HINSTANCE g_hInst;
using namespace Gdiplus;
Sprite::Sprite(int imageID, int maskID)
{
	// Load the bitmap resources.
	mhImage = LoadBitmap(g_hInst, MAKEINTRESOURCE(imageID));
	mhMask = LoadBitmap(g_hInst, MAKEINTRESOURCE(maskID));

	// Get the BITMAP structure for each of the bitmaps.
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	GetObject(mhMask, sizeof(BITMAP), &mMaskBM);

	// Image and Mask should be the same dimensions.
	assert(mImageBM.bmWidth == mMaskBM.bmWidth);
	assert(mImageBM.bmHeight == mMaskBM.bmHeight);

	mcTransparentColor = 0;
	mhSpriteDC = 0;
}

Sprite::Sprite(const char *szImageFile, const char *szMaskFile)
{
	mhImage = (HBITMAP)LoadImage(g_hInst, szImageFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);
	mhMask = (HBITMAP)LoadImage(g_hInst, szMaskFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);

	// Get the BITMAP structure for each of the bitmaps.
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	GetObject(mhMask, sizeof(BITMAP), &mMaskBM);

	// Image and Mask should be the same dimensions.
	assert(mImageBM.bmWidth == mMaskBM.bmWidth);
	assert(mImageBM.bmHeight == mMaskBM.bmHeight);

	mcTransparentColor = 0;
	mhSpriteDC = 0;
}

Sprite::Sprite(const char *szImageFile, COLORREF crTransparentColor, int rotation_angle)
{
	mhImage = (HBITMAP)LoadImage(g_hInst, szImageFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);

	mhMask = 0;
	mhSpriteDC = 0;
	mcTransparentColor = crTransparentColor;

	// Get the BITMAP structure for the bitmap.
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);

	auxmhImage = mhImage;

	angle = rotation_angle;
	if(angle != 0) {
		mhImage = GetRotatedBitmapNT(mhImage, DEG2RAD(angle), mcTransparentColor);

		GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	}
	int raza = width() / 2;
	double radians = degreeToRadians(angle);
	Vec2 plane_apex = Polar(raza, radians);

	set_apex(plane_apex);

}

void Sprite::CreateRotationFrames() {
	int i = 0;
	//rotated_images = new HBITMAP[360 / ANGLE_ROTATION];
	for (int angle = 0; angle < 360; angle += ANGLE_ROTATION, i++) {
		rotated_images.push_back(GetRotatedBitmapNT(mhImage, DEG2RAD(angle), RGB(255, 0, 255)));
	}
}


Sprite::~Sprite()
{
	// Free the resources we created in the constructor.
	DeleteObject(mhImage);
	DeleteObject(mhMask);

	DeleteDC(mhSpriteDC);
	//delete[] rotated_images;
}

void Sprite::update(float dt)
{
	// Update the sprites position.

	mPosition += mVelocity * dt;
	// Update bounding rectangle/circle
}

void Sprite::setBackBuffer(const BackBuffer *pBackBuffer)
{
	mpBackBuffer = pBackBuffer;
	if (mpBackBuffer)
	{
		DeleteDC(mhSpriteDC);
		mhSpriteDC = CreateCompatibleDC(mpBackBuffer->getDC());
	}
}

void Sprite::draw()
{
	if (mhMask != 0)
		drawMask();
	else
		drawTransparent();
}

void Sprite::drawMask()
{
	if (mpBackBuffer == NULL)
		return;

	HDC hBackBufferDC = mpBackBuffer->getDC();

	// The position BitBlt wants is not the sprite's center
	// position; rather, it wants the upper-left position,
	// so compute that.
	int w = width();
	int h = height();

	// Upper-left corner.
	int x = (int)mPosition.x - (w / 2);
	int y = (int)mPosition.y - (h / 2);

	// Note: For this masking technique to work, it is assumed
	// the backbuffer bitmap has been cleared to some
	// non-zero value.
	// Select the mask bitmap.
	HGDIOBJ oldObj = SelectObject(mhSpriteDC, mhMask);

	// Draw the mask to the backbuffer with SRCAND. This
	// only draws the black pixels in the mask to the backbuffer,
	// thereby marking the pixels we want to draw the sprite
	// image onto.
	BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, 0, 0, SRCAND);

	// Now select the image bitmap.
	SelectObject(mhSpriteDC, mhImage);

	// Draw the image to the backbuffer with SRCPAINT. This
	// will only draw the image onto the pixels that where previously
	// marked black by the mask.
	BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, 0, 0, SRCPAINT);

	// Restore the original bitmap object.
	SelectObject(mhSpriteDC, oldObj);
}

vector<unsigned char> Sprite::ToPixels(HBITMAP BitmapHandle, int &width, int &height)
{
	BITMAP Bmp = { 0 };
	BITMAPINFO Info = { 0 };
	std::vector<unsigned char> Pixels = std::vector<unsigned char>();

	HDC DC = CreateCompatibleDC(NULL);
	std::memset(&Info, 0, sizeof(BITMAPINFO)); //not necessary really..
	HBITMAP OldBitmap = (HBITMAP)SelectObject(DC, BitmapHandle);
	GetObject(BitmapHandle, sizeof(Bmp), &Bmp);

	Info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	Info.bmiHeader.biWidth = width = Bmp.bmWidth;
	Info.bmiHeader.biHeight = height = Bmp.bmHeight;
	Info.bmiHeader.biPlanes = 1;
	Info.bmiHeader.biBitCount = Bmp.bmBitsPixel;
	Info.bmiHeader.biCompression = BI_RGB;
	Info.bmiHeader.biSizeImage = ((width * Bmp.bmBitsPixel + 31) / 32) * 4 * height;

	Pixels.resize(Info.bmiHeader.biSizeImage);
	GetDIBits(DC, BitmapHandle, 0, height, &Pixels[0], &Info, DIB_RGB_COLORS);
	SelectObject(DC, OldBitmap);

	height = std::abs(height);
	DeleteDC(DC);
	return Pixels;
}

bool Sprite::isSamePixelColor(const COLORREF first_pixel, const COLORREF second_pixel) {
	COLORREF aux_first_pixel = first_pixel & 0xFFFFFF;
	COLORREF aux_second_pixel = second_pixel & 0xFFFFFF;
	return aux_first_pixel == aux_second_pixel;
}

void Sprite::UpdatePixelBuffer() {
	int image_width = width();
	int image_height = height();
	//BYTE *pData;

	//BITMAP bitmap;
	//GetObject(mhImage, sizeof(BITMAP), &bitmap);
	//BITMAP* ptrBitmap = &bitmap;
	//const BITMAP* cBitmap = const_cast<BITMAP*> (ptrBitmap);
	//HBITMAP bitmapTrans = CreateBitmap(bitmap.bmWidth, bitmap.bmHeight, 1, 1, NULL);
	//HBITMAP hbitmap = CreateBitmapIndirect(cBitmap);
	//image_pixels.resize(image_width * image_height);
	image_pixels.resize(0);
	image_pixels.resize(image_width* image_height,1);
	BITMAPINFO bitmapInfo = { 0 };
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biWidth = image_width;
	bitmapInfo.bmiHeader.biHeight = image_height;
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biBitCount = 32;
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biSizeImage = 0;

	GetDIBits(mhSpriteDC, mhImage, 0, 0, NULL, &bitmapInfo, DIB_RGB_COLORS);

	//bitmapInfo.bmiHeader.biBitCount = 32;
	//COLORREF* pixel = new COLORREF[width * height];

	if(GetDIBits(mhSpriteDC, mhImage, 0, image_height, &image_pixels[0], &bitmapInfo, DIB_RGB_COLORS) != image_height) {
		int a;
	}
}

bool Sprite::isApexInsideSprite(const Sprite* other) {
	bool width_collision = false;
	bool height_collision = false;

	int half_width_dimension = width() / 2;
	int half_height_dimension = height() / 2;

	if(mPosition.x + half_width_dimension >= other->mPosition.x + other->apex.x &&
		mPosition.x - half_width_dimension <= other->mPosition.x + other->apex.x) {
		width_collision = true;
	}

	if(mPosition.y - half_height_dimension <= other->mPosition.y - other->apex.y && 
		mPosition.y + half_height_dimension >= other->mPosition.y - other->apex.y) {
		height_collision = true;
	}
	return width_collision && height_collision;
}

bool Sprite::isPointInsideSprite(const Vec2 point) {
	bool width_collision = false;
	bool height_collision = false;

	int half_width_dimension = width() / 2;
	int half_height_dimension = height() / 2;

	if (mPosition.x - half_width_dimension >= point.x && 
		mPosition.x + half_width_dimension <= point.x) {
		width_collision = true;
	}
	if (mPosition.y - half_height_dimension >= point.y && 
		mPosition.y + half_height_dimension <= point.y) {
		height_collision = true;
	}
	//if (mPosition.x + width() / 2 > other->mPosition.x - other->width() / 2 &&
	//	mPosition.x - width() / 2 < other->mPosition.x + other->width() / 2)
	//	widthCollision = true;
	return width_collision && height_collision;
}

bool Sprite::isCollision(const Sprite* other)
{
	/*Graphics a(mpBackBuffer->getDC());

	Bitmap myBitmap(L"data/PlaneImg.bmp");

	int w = myBitmap.GetWidth();*/
	//int h = myBitmap.GetHeight();

	//Color pixColor;
	//myBitmap.GetPixel(50, 50, &pixColor);
	//int r = pixColor.GetRed();
	//int g = pixColor.GetGreen();
	//int b = pixColor.GetBlue();

	//cout << "pixColor " << r << endl;




	//COLORREF acr[300];
	/*
	for(int i = 0; i < 100; i+=3) {
		acr[i] = GetRValue(pixel[6287]);
		acr[i+1] = GetGValue(pixel[6287]);
		acr[i+2] = GetBValue(pixel[6287]);
	}*/

	//int wid = width();
	//int hei = height();
	//BYTE *pData;

	/*BITMAP bitmap;
	GetObject(mhImage, sizeof(BITMAP), &bitmap);
	BITMAP* ptrBitmap = &bitmap;
	const BITMAP* cBitmap = const_cast<BITMAP*> (ptrBitmap);
	HBITMAP bitmapTrans = CreateBitmap(bitmap.bmWidth, bitmap.bmHeight, 1, 1, NULL);
	HBITMAP hbitmap = CreateBitmapIndirect(cBitmap);
*/
	//BITMAPINFO bitmapInfo = { 0 };
	//bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	//bitmapInfo.bmiHeader.biWidth = wid;
	//bitmapInfo.bmiHeader.biHeight = hei;
	//bitmapInfo.bmiHeader.biPlanes = 1;
	//bitmapInfo.bmiHeader.biBitCount = 24;
	//bitmapInfo.bmiHeader.biCompression = BI_RGB;
	//bitmapInfo.bmiHeader.biSizeImage = 0;
	//int returnVal1 = GetDIBits(mhSpriteDC, mhImage, 0, 0, NULL, &bitmapInfo, DIB_RGB_COLORS);

	////COLORREF* pixel = new COLORREF[wid * hei];
	//vector<COLORREF> pixe;
	//pixe.resize(wid * hei);
	//int returnVal = GetDIBits(mhSpriteDC, mhImage, 0, height(), &pixe[0], &bitmapInfo, DIB_RGB_COLORS);
	//UpdatePixelBuffer();


	//int acr[30];
	//for(int i = 0; i < 30; i+=3) {
	//	acr[i] = GetRValue(image_pixels[6250]);
	//	acr[i+1] = GetGValue(image_pixels[6250]);
	//	acr[i+2] = GetBValue(image_pixels[6250]);
	//}

	//delete[] pixel;
	//cBitmap = nullptr;
	//delete cBitmCap;
	//BITMAPINFOHEADER MyBMInfo;// = new BITMAPINFOHEADER;
	//						  //MyBMInfo.biSize = sizeof(MyBMInfo);
	//ZeroMemory(&MyBMInfo, sizeof(BITMAPINFO));
	//MyBMInfo.biSize = sizeof(BITMAPINFOHEADER);
	//// Get the BITMAPINFO structure from the bitmap
	//if (0 == GetDIBits(mhSpriteDC, bitmapTrans, 0, 0, NULL, (BITMAPINFO*)&MyBMInfo, DIB_RGB_COLORS))
	//{
	//	// error handling
	//}

	//// create the pixel buffer
	////BYTE* Pixels = new BYTE[MyBMInfo->biSizeImage];
	////vector<unsigned char> pix;// = new vector<unsigned char>();
	//// We'll change the received BITMAPINFOHEADER to request the data in a
	//// 32 bit RGB format (and not upside-down) so that we can iterate over
	//// the pixels easily. 

	//// requesting a 32 bit image means that no stride/padding will be necessary,
	//// although it always contains an (possibly unused) alpha channel


	//MyBMInfo.biBitCount = 24;
	//MyBMInfo.biCompression = BI_RGB;  // no compression -> easier to use
	//								  // correct the bottom-up ordering of lines (abs is in cstdblib and stdlib.h)
	//MyBMInfo.biHeight = abs(MyBMInfo.biHeight);
	////pix.resize(4 * width() * height());
	//pData = new BYTE[MyBMInfo.biSizeImage];
	//int aux1 = GetDIBits(mhSpriteDC, bitmapTrans, 0, hei, pData, (BITMAPINFO*)&MyBMInfo, DIB_RGB_COLORS);


	//// release previously loaded file data

	///*BITMAPINFOHEADER m_biInfo;

	//ZeroMemory(&m_biInfo, sizeof(BITMAPINFO));
	//m_biInfo.biSize = sizeof(BITMAPINFOHEADER);

	//GetDIBits(mhSpriteDC, bitmapTrans, 0, 0, NULL, (BITMAPINFO*)&m_biInfo, DIB_RGB_COLORS);
	//m_biInfo.biBitCount = 24;
	//m_biInfo.biCompression = BI_RGB;
	//pData = new BYTE[m_biInfo.biSizeImage];


	//GetDIBits(mhSpriteDC, bitmapTrans, 0, hei, pData, (BITMAPINFO*)&m_biInfo, DIB_RGB_COLORS);
	//*/



	//int PixelX = 31;
	//int PixelY = 50;
	//int a[10000];
	//for (int i = 0; i <= 10000; i++) {
	//	a[i] = pData[i];
	//}
	////int r = Pixels[3];
	////int g = Pixels[4];
	////int b = Pixels[5];
	//int aux = 3 * ((PixelX - 1) + (((hei - 1) - PixelY) - 1)*wid);
	//int r = (int)pData[aux];
	//int g = (int)pData[aux + 1];
	//int b = (int)pData[aux + 2];
	//pData = NULL;
	//delete[] pData;
	//Vec2 relative_apex;
	//
	//relative_apex.x = other->mPosition.x + other->apex.x;
	//relative_apex.y = other->mPosition.y - other->apex.y;
	//std::vector<Vec2> vecaux;
	//long int aux = 0;
	////SetPixel(mhSpriteDC, relative_apex.x, relative_apex.y, RGB(0, 225, 0));
	//for(int i = 0; i < buffer_width(); i++) {
	//	for(int j = 0; j <= 1; j++) {
	//		auto test = GetPixel(mhSpriteDC, 300, 600);
	//		test = test & 0xFFFFFF;
	//		if(test == RGB(255,255,255)) {
	//			aux++;
	//		}
	//	}
	//}
	//auto test = GetPixel(mhSpriteDC, 300, 600);
	//test = test & 0xFFFFFF;
	//std::cout << test << "\n";
	////std::cout << relative_apex.x << " " << relative_apex.y << "\n";

	//TransformAndDraw(5, mpBackBuffer->getHWND());

	/*XFORM xForm;
	xForm.eM11 = (FLOAT) 0.8660;
	xForm.eM12 = (FLOAT) 0.5000;
	xForm.eM21 = (FLOAT)-0.5000;
	xForm.eM22 = (FLOAT) 0.8660;
	xForm.eDx = (FLOAT) 0.0;
	xForm.eDy = (FLOAT) 0.0;
	SetWorldTransform(mpBackBuffer->getDC(), &xForm);
*/
	//ReleaseDC(hWnd, hDC);

	//Rotate(2, "PlaneImgAndMask1.bmp");

	
	//int b = other_apex_relative.x;
	//if(other_apex_relative.x < 1 && other_apex_relative.x >= 0) {
	//	int a;
	//}
	//if(isApexInsideSprite(other)) {
	//	int c;
	//	c++;
	//}


	if (isApexInsideSprite(other)) {
		//other_apex_relative.x = other->mPosition.x + other_apex_relative.x;
		//other_apex_relative.y = other->mPosition.y - other_apex_relative.y;
		//Vec2 bottom_right_point_relative(mPosition.x + width() / 2, mPosition.y + height() / 2);
		//Vec2 pixel_to_check = bottom_right_point_relative - other_apex_relative;
		//pixel_to_check.x = width() - pixel_to_check.x;
		//pixel_to_check.y = height() - pixel_to_check.y;
		//cout << "true" << endl;
		///*vector<int> pixnonzero;
		//for(int i = 0; i < image_pixels.size(); i++) {
		//	if(isSamePixelColor(image_pixels[i], mcTransparentColor)) {
		//		pixnonzero.push_back(i);
		//	}
		//}*/
		//int PixelX = pixel_to_check.x;
		//int PixelY = pixel_to_check.y;
		//int wid = width();// -width() / 4;
		////int posi = (height() - PixelY - 1) * wid + PixelX * 3 / 4;

		//int posi = (height() - PixelY - 1) * wid + PixelX;
		//int nIndex = (PixelY * height()) + PixelX;
		//int aux2 = ((PixelX - 1) + (((height() - 1) - PixelY) - 1)*width());
		//int aux = ((pixel_to_check.x - 1) + (((height() - 1) - pixel_to_check.y) - 1)*width());7
		int pixel_index = GetPixelToCheck(other);
		COLORREF color = image_pixels[pixel_index];
		if(isSamePixelColor(color, mcTransparentColor))
			return false;
		else 
			return true;
	}
	return false;
	//bool widthCollision = false;
	//bool heightCollision = false;

	//if (mPosition.x + width() / 2 > other->mPosition.x - other->width() / 2 &&
	//	mPosition.x - width() / 2 < other->mPosition.x + other->width() / 2)
	//	widthCollision = true;

	//if (mPosition.y + height() / 2 > other->mPosition.y - other->height() / 2 &&
	//	mPosition.y - height() / 2 < other->mPosition.y + other->height() / 2)
	//	heightCollision = true;

	////delete MyBMInfo;
	//return (heightCollision && widthCollision);
}

int Sprite::GetPixelToCheck(const Sprite* other) {

	int _width = width();
	int _height = height();
	
	Vec2 other_apex_relative = other->get_apex();
	
	other_apex_relative.x = other->mPosition.x + other_apex_relative.x;
	other_apex_relative.y = other->mPosition.y - other_apex_relative.y;
	
	Vec2 bottom_right_point_relative(mPosition.x + _width / 2, mPosition.y + _height / 2);
	Vec2 pixel_to_check = bottom_right_point_relative - other_apex_relative;
	
	pixel_to_check.x = _width - pixel_to_check.x;
	pixel_to_check.y = _height - pixel_to_check.y;
	
	int PixelX = pixel_to_check.x;
	int PixelY = pixel_to_check.y;
	
	return (_height - PixelY - 1) * _width + PixelX;
}

void Sprite::drawTransparent()
{
	if (mpBackBuffer == NULL)
		return;

	HDC hBackBuffer = mpBackBuffer->getDC();

	int w = width();
	int h = height();

	// Upper-left corner.
	int x = (int)mPosition.x - (w / 2);
	int y = (int)mPosition.y - (h / 2);

	COLORREF crOldBack = SetBkColor(hBackBuffer, RGB(255, 255, 255));
	COLORREF crOldText = SetTextColor(hBackBuffer, RGB(0, 0, 0));
	HDC dcImage, dcTrans;

	// Create two memory dcs for the image and the mask
	dcImage = CreateCompatibleDC(hBackBuffer);
	dcTrans = CreateCompatibleDC(hBackBuffer);

	// Select the image into the appropriate dc
	SelectObject(dcImage, mhImage);

	// Create the mask bitmap
	BITMAP bitmap;
	GetObject(mhImage, sizeof(BITMAP), &bitmap);
	HBITMAP bitmapTrans = CreateBitmap(bitmap.bmWidth, bitmap.bmHeight, 1, 1, NULL);

	// Select the mask bitmap into the appropriate dc
	SelectObject(dcTrans, bitmapTrans);

	// Build mask based on transparent color
	SetBkColor(dcImage, mcTransparentColor);
	BitBlt(dcTrans, 0, 0, bitmap.bmWidth, bitmap.bmHeight, dcImage, 0, 0, SRCCOPY);

	// Do the work - True Mask method - cool if not actual display
	BitBlt(hBackBuffer, x, y, bitmap.bmWidth, bitmap.bmHeight, dcImage, 0, 0, SRCINVERT);
	BitBlt(hBackBuffer, x, y, bitmap.bmWidth, bitmap.bmHeight, dcTrans, 0, 0, SRCAND);
	BitBlt(hBackBuffer, x, y, bitmap.bmWidth, bitmap.bmHeight, dcImage, 0, 0, SRCINVERT);

	// free memory	
	DeleteDC(dcImage);
	DeleteDC(dcTrans);
	DeleteObject(bitmapTrans);

	// Restore settings
	SetBkColor(hBackBuffer, crOldBack);
	SetTextColor(hBackBuffer, crOldText);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Sprite::computeVelocity() {
	mVelocity.x = MAX_SPEED * cos(DEG2RAD(angle));
	mVelocity.y = -MAX_SPEED * sin(DEG2RAD(angle));
}

void Sprite::computeCenter() {

}

string Sprite::toString() {
	string result;

	result = to_string(mPosition.x) + " " + to_string(mPosition.y) + " " + to_string(angle) + "\n";

	return result;
}

AnimatedSprite::AnimatedSprite(const char *szImageFile, const char *szMaskFile, const RECT& rcFirstFrame, int iFrameCount)
	: Sprite(szImageFile, szMaskFile)
{
	mptFrameCrop.x = rcFirstFrame.left;
	mptFrameCrop.y = rcFirstFrame.top;
	mptFrameStartCrop = mptFrameCrop;
	miFrameWidth = rcFirstFrame.right - rcFirstFrame.left;
	miFrameHeight = rcFirstFrame.bottom - rcFirstFrame.top;
	miFrameCount = iFrameCount;
}

void AnimatedSprite::SetFrame(int iIndex)
{
	// index must be in range
	assert(iIndex >= 0 && iIndex < miFrameCount && "AnimatedSprite frame Index must be in range!");
	//mptFrameCrop.y = 0;
	//mptFrameCrop.x = 256;
	int noOfFrames = 4;

	int rand = iIndex / noOfFrames;
	int coloana = iIndex % noOfFrames;
	mptFrameCrop.x = mptFrameStartCrop.x + coloana*miFrameWidth;
	mptFrameCrop.y = mptFrameStartCrop.y + rand*miFrameHeight;

	//scrie in consola
	AllocConsole();
	freopen("conin$", "r", stdin);
	freopen("conout$", "w", stdout);
	freopen("conout$", "w", stderr);
	//printf("Debugging Window:\n");
	std::cout << mptFrameCrop.x << " " << mptFrameCrop.y << std::endl;
	//mptFrameStartCrop.x = mptFrameCrop.x;
	//mptFrameStartCrop.y = mptFrameCrop.y;
}

void AnimatedSprite::draw()
{
	if (mpBackBuffer == NULL)
		return;

	// The position BitBlt wants is not the sprite's center
	// position; rather, it wants the upper-left position,
	// so compute that.
	int w = miFrameWidth;
	int h = miFrameHeight;

	HDC hBackBufferDC = mpBackBuffer->getDC();

	// Upper-left corner.
	int x = (int)mPosition.x - (w / 2);
	int y = (int)mPosition.y - (h / 2);

	// Note: For this masking technique to work, it is assumed
	// the backbuffer bitmap has been cleared to some
	// non-zero value.
	// Select the mask bitmap.
	HGDIOBJ oldObj = SelectObject(mhSpriteDC, mhMask);

	// Draw the mask to the backbuffer with SRCAND. This
	// only draws the black pixels in the mask to the backbuffer,
	// thereby marking the pixels we want to draw the sprite
	// image onto.

	//BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, 256, 256, SRCAND);
	BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, mptFrameCrop.x, mptFrameCrop.y, SRCAND);

	// Now select the image bitmap.
	SelectObject(mhSpriteDC, mhImage);

	// Draw the image to the backbuffer with SRCPAINT. This
	// will only draw the image onto the pixels that where previously
	// marked black by the mask.

	//BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, 256, 256, SRCPAINT);
	BitBlt(hBackBufferDC, x, y, w, h, mhSpriteDC, mptFrameCrop.x, mptFrameCrop.y, SRCPAINT);


	// Restore the original bitmap object.
	SelectObject(mhSpriteDC, oldObj);
}
//
//void Sprite::TransformAndDraw(INT iTransform, HWND hWnd)
//{
//	HDC hDC;
//	XFORM xForm;
//	RECT rect;
//
//	// Retrieve a DC HANDLE FOR the application's window.
//
//	hDC = GetDC(hWnd);
//
//	// SET the mapping mode TO LOENGLISH. THIS moves the
//	// CLIENT area origin FROM the upper LEFT corner OF the
//	// WINDOW TO the lower LEFT corner (THIS also reorients
//	// the y-axis so that drawing operations occur IN a true
//	// Cartesian space). It guarantees portability so that
//	// the OBJECT drawn retains its dimensions ON ANY display.
//
//	SetGraphicsMode(hDC, GM_ADVANCED);
//	SetMapMode(hDC, MM_LOENGLISH);
//
//	// SET the appropriate world transformation (based ON the
//	// user's menu selection).
//
//	switch(iTransform)
//	{
//		
//	case 1:        // Scale to 1/2 of the original size.  
//		xForm.eM11 = (FLOAT) 0.5;
//		xForm.eM12 = (FLOAT) 0.0;
//		xForm.eM21 = (FLOAT) 0.0;
//		xForm.eM22 = (FLOAT) 0.5;
//		xForm.eDx = (FLOAT) 0.0;
//		xForm.eDy = (FLOAT) 0.0;
//		SetWorldTransform(hDC, &xForm);
//		break;
//
//	case 2:   // Translate right by 3/4 inch.  
//		xForm.eM11 = (FLOAT) 1.0;
//		xForm.eM12 = (FLOAT) 0.0;
//		xForm.eM21 = (FLOAT) 0.0;
//		xForm.eM22 = (FLOAT) 1.0;
//		xForm.eDx = (FLOAT) 75.0;
//		xForm.eDy = (FLOAT) 0.0;
//		SetWorldTransform(hDC, &xForm);
//		break;
//
//	case 3:      // Rotate 30 degrees counterclockwise.  
//		xForm.eM11 = (FLOAT) 0.8660;
//		xForm.eM12 = (FLOAT) 0.5000;
//		xForm.eM21 = (FLOAT)-0.5000;
//		xForm.eM22 = (FLOAT) 0.8660;
//		xForm.eDx = (FLOAT) 0.0;
//		xForm.eDy = (FLOAT) 0.0;
//		SetWorldTransform(hDC, &xForm);
//		break;
//
//	case 4:       // Shear along the x-axis with a  
//					  // proportionality constant of 1.0.  
//		xForm.eM11 = (FLOAT) 1.0;
//		xForm.eM12 = (FLOAT) 1.0;
//		xForm.eM21 = (FLOAT) 0.0;
//		xForm.eM22 = (FLOAT) 1.0;
//		xForm.eDx = (FLOAT) 0.0;
//		xForm.eDy = (FLOAT) 0.0;
//		SetWorldTransform(hDC, &xForm);
//		break;
//
//	case 5:     // Reflect about a horizontal axis.  
//		xForm.eM11 = (FLOAT) 1.0;
//		xForm.eM12 = (FLOAT) 0.0;
//		xForm.eM21 = (FLOAT) 0.0;
//		xForm.eM22 = (FLOAT)-1.0;
//		xForm.eDx = (FLOAT) 0.0;
//		xForm.eDy = (FLOAT) 0.0;
//		SetWorldTransform(hDC, &xForm);
//		break;
//
//	case 6:      // Set the unity transformation.  
//		xForm.eM11 = (FLOAT) 1.0;
//		xForm.eM12 = (FLOAT) 0.0;
//		xForm.eM21 = (FLOAT) 0.0;
//		xForm.eM22 = (FLOAT) 1.0;
//		xForm.eDx = (FLOAT) 0.0;
//		xForm.eDy = (FLOAT) 0.0;
//		SetWorldTransform(hDC, &xForm);
//		break;
//
//	}
//
//	// FIND the midpoint OF the CLIENT area.
//
//	GetClientRect(hWnd, (LPRECT)&rect);
//	DPtoLP(hDC, (LPPOINT)&rect, 2);
//
//	// SELECT a hollow brush.
//
//	SelectObject(hDC, GetStockObject(HOLLOW_BRUSH));
//
//	// Draw the exterior circle.
//
//	Ellipse(hDC, (rect.right / 2 - 100), (rect.bottom / 2 + 100),
//		(rect.right / 2 + 100), (rect.bottom / 2 - 100));
//
//	// Draw the interior circle.
//
//	Ellipse(hDC, (rect.right / 2 - 94), (rect.bottom / 2 + 94),
//		(rect.right / 2 + 94), (rect.bottom / 2 - 94));
//
//	// Draw the key.
//
//	Rectangle(hDC, (rect.right / 2 - 13), (rect.bottom / 2 + 113),
//		(rect.right / 2 + 13), (rect.bottom / 2 + 50));
//	Rectangle(hDC, (rect.right / 2 - 13), (rect.bottom / 2 + 96),
//		(rect.right / 2 + 13), (rect.bottom / 2 + 50));
//
//	// Draw the horizontal lines.
//
//	MoveToEx(hDC, (rect.right / 2 - 150), (rect.bottom / 2 + 0), NULL);
//	LineTo(hDC, (rect.right / 2 - 16), (rect.bottom / 2 + 0));
//
//	MoveToEx(hDC, (rect.right / 2 - 13), (rect.bottom / 2 + 0), NULL);
//	LineTo(hDC, (rect.right / 2 + 13), (rect.bottom / 2 + 0));
//
//	MoveToEx(hDC, (rect.right / 2 + 16), (rect.bottom / 2 + 0), NULL);
//	LineTo(hDC, (rect.right / 2 + 150), (rect.bottom / 2 + 0));
//
//	// Draw the vertical lines.
//
//	MoveToEx(hDC, (rect.right / 2 + 0), (rect.bottom / 2 - 150), NULL);
//	LineTo(hDC, (rect.right / 2 + 0), (rect.bottom / 2 - 16));
//
//	MoveToEx(hDC, (rect.right / 2 + 0), (rect.bottom / 2 - 13), NULL);
//	LineTo(hDC, (rect.right / 2 + 0), (rect.bottom / 2 + 13));
//
//	MoveToEx(hDC, (rect.right / 2 + 0), (rect.bottom / 2 + 16), NULL);
//	LineTo(hDC, (rect.right / 2 + 0), (rect.bottom / 2 + 150));
//
//	ReleaseDC(hWnd, hDC);
//}
//
//HBITMAP Sprite::GetRotatedBitmapNT(HBITMAP hBitmap, float radians, COLORREF clrBack)
//{
//	// Create a memory DC compatible with the display
//	CDC::CDC sourceDC, destDC;
//	sourceDC.CreateCompatibleDC(NULL);
//	destDC.CreateCompatibleDC(NULL);
//
//	// Get logical coordinates
//	BITMAP bm;
//	::GetObject(hBitmap, sizeof(bm), &bm);
//
//	float cosine = (float)cos(radians);
//	float sine = (float)sin(radians);
//
//	// Compute dimensions of the resulting bitmap
//	// First get the coordinates of the 3 corners other than origin
//	int x1 = (int)(bm.bmHeight * sine);
//	int y1 = (int)(bm.bmHeight * cosine);
//	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine);
//	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine);
//	int x3 = (int)(bm.bmWidth * cosine);
//	int y3 = (int)(-bm.bmWidth * sine);
//
//	int minx = min(0, min(x1, min(x2, x3)));
//	int miny = min(0, min(y1, min(y2, y3)));
//	int maxx = max(0, max(x1, max(x2, x3)));
//	int maxy = max(0, max(y1, max(y2, y3)));
//
//	int w = maxx - minx;
//	int h = maxy - miny;
//
//	// Create a bitmap to hold the result
//	HBITMAP hbmResult = ::CreateCompatibleBitmap(CClientDC(NULL), w, h);
//
//	HBITMAP hbmOldSource = (HBITMAP)::SelectObject(sourceDC.m_hDC, hBitmap);
//	HBITMAP hbmOldDest = (HBITMAP)::SelectObject(destDC.m_hDC, hbmResult);
//
//	// Draw the background color before we change mapping mode
//	HBRUSH hbrBack = CreateSolidBrush(clrBack);
//	HBRUSH hbrOld = (HBRUSH)::SelectObject(destDC.m_hDC, hbrBack);
//	destDC.PatBlt(0, 0, w, h, PATCOPY);
//	::DeleteObject(::SelectObject(destDC.m_hDC, hbrOld));
//
//	// We will use world transform to rotate the bitmap
//	SetGraphicsMode(destDC.m_hDC, GM_ADVANCED);
//	XFORM xform;
//	xform.eM11 = cosine;
//	xform.eM12 = -sine;
//	xform.eM21 = sine;
//	xform.eM22 = cosine;
//	xform.eDx = (float)-minx;
//	xform.eDy = (float)-miny;
//
//	SetWorldTransform(destDC.m_hDC, &xform);
//
//	// Now do the actual rotating - a pixel at a time
//	destDC.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &sourceDC, 0, 0, SRCCOPY);
//
//	// Restore DCs
//	::SelectObject(sourceDC.m_hDC, hbmOldSource);
//	::SelectObject(destDC.m_hDC, hbmOldDest);
//
//	return hbmResult;
//}

HBITMAP Sprite::GetRotatedBitmapNT(HBITMAP hBitmap, float radians, COLORREF clrBack)
{
	// Create a memory DC compatible with the display
	CDC sourceDC, destDC;
	sourceDC.CreateCompatibleDC(NULL);
	destDC.CreateCompatibleDC(NULL);

	// Get logical coordinates
	BITMAP bm;
	::GetObject(hBitmap, sizeof(bm), &bm);

	float cosine = (float)cos(radians);
	float sine = (float)sin(radians);

	// Compute dimensions of the resulting bitmap
	// First get the coordinates of the 3 corners other than origin
	int x1 = (int)(bm.bmHeight * sine);
	int y1 = (int)(bm.bmHeight * cosine);
	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine);
	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine);
	int x3 = (int)(bm.bmWidth * cosine);
	int y3 = (int)(-bm.bmWidth * sine);

	int minx = min(0, min(x1, min(x2, x3)));
	int miny = min(0, min(y1, min(y2, y3)));
	int maxx = max(0, max(x1, max(x2, x3)));
	int maxy = max(0, max(y1, max(y2, y3)));

	int w = maxx - minx;
	int h = maxy - miny;

	// Create a bitmap to hold the result
	HBITMAP hbmResult = ::CreateCompatibleBitmap(CClientDC(NULL), w, h);

	HBITMAP hbmOldSource = (HBITMAP)::SelectObject(sourceDC.m_hDC, hBitmap);
	HBITMAP hbmOldDest = (HBITMAP)::SelectObject(destDC.m_hDC, hbmResult);

	// Draw the background color before we change mapping mode
	HBRUSH hbrBack = CreateSolidBrush(clrBack);
	HBRUSH hbrOld = (HBRUSH)::SelectObject(destDC.m_hDC, hbrBack);
	destDC.PatBlt(0, 0, w, h, PATCOPY);
	::DeleteObject(::SelectObject(destDC.m_hDC, hbrOld));

	// We will use world transform to rotate the bitmap
	SetGraphicsMode(destDC.m_hDC, GM_ADVANCED);
	XFORM xform;
	xform.eM11 = cosine;
	xform.eM12 = -sine;
	xform.eM21 = sine;
	xform.eM22 = cosine;
	xform.eDx = (float)-minx;
	xform.eDy = (float)-miny;

	SetWorldTransform(destDC.m_hDC, &xform);

	// Now do the actual rotating - a pixel at a time
	destDC.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &sourceDC, 0, 0, SRCCOPY);

	// Restore DCs
	::SelectObject(sourceDC.m_hDC, hbmOldSource);
	::SelectObject(destDC.m_hDC, hbmOldDest);

	return hbmResult;
}

void Sprite::Pixel() {
	
}

void Sprite::Rotate(int angle) {
	mhImage = GetRotatedBitmapNT(auxmhImage, DEG2RAD(angle), mcTransparentColor);
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	UpdatePixelBuffer();
	int raza = width() / 2;
	double radians = degreeToRadians(angle);
	Vec2 plane_apex = Polar(raza, radians);

	set_apex(plane_apex);
	/*
	cout << "old pos " << mPosition.x << " " << mPosition.y << endl;
	int frame = angle / ANGLE_ROTATION;
	mhImage = rotated_images[frame];

	cout << "new pos " << mPosition.x << " " << mPosition.y << endl;
	cout << "old dim " << width() << " " << height()<< endl;
	GetObject(mhImage, sizeof(BITMAP), &mImageBM);
	cout << "new dim " << width() << " " << height() << endl;*/
}

void Sprite::Rotate(float radians, const char* szImageFile)
{
	// Create a memory DC compatible with the display
	HDC sourceDC, destDC;
	sourceDC = CreateCompatibleDC(mpBackBuffer->getDC());
	destDC = CreateCompatibleDC(mpBackBuffer->getDC());

	// Get logical coordinates
	HBITMAP hBitmap = (HBITMAP)LoadImage(g_hInst, szImageFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);
	BITMAP bm;
	::GetObject(hBitmap, sizeof(bm), &bm);

	float cosine = (float)cos(radians);
	float sine = (float)sin(radians);

	// Compute dimensions of the resulting bitmap
	// First get the coordinates of the 3 corners other than origin
	int x1 = (int)(bm.bmHeight * sine);
	int y1 = (int)(bm.bmHeight * cosine);
	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine);
	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine);
	int x3 = (int)(bm.bmWidth * cosine);
	int y3 = (int)(-bm.bmWidth * sine);

	int minx = min(0, min(x1, min(x2, x3)));
	int miny = min(0, min(y1, min(y2, y3)));
	int maxx = max(0, max(x1, max(x2, x3)));
	int maxy = max(0, max(y1, max(y2, y3)));

	int w = maxx - minx;
	int h = maxy - miny;

	// Create a bitmap to hold the result
	HBITMAP hbmResult = ::CreateCompatibleBitmap(mpBackBuffer->getDC(), w, h);

	HBITMAP hbmOldSource = (HBITMAP)::SelectObject(sourceDC, hBitmap);
	HBITMAP hbmOldDest = (HBITMAP)::SelectObject(destDC, hbmResult);

	// Draw the background color before we change mapping mode
	HBRUSH hbrBack = CreateSolidBrush(mcTransparentColor);
	HBRUSH hbrOld = (HBRUSH)::SelectObject(destDC, hbrBack);
	PatBlt(destDC, 0, 0, w, h, PATCOPY);
	::DeleteObject(::SelectObject(destDC, hbrOld));




	// We will use world transform to rotate the bitmap
	SetGraphicsMode(destDC, GM_ADVANCED);
	XFORM xform;
	xform.eM11 = cosine;
	xform.eM12 = -sine;
	xform.eM21 = sine;
	xform.eM22 = cosine;
	xform.eDx = (float)-minx;
	xform.eDy = (float)-miny;

	SetWorldTransform(destDC, &xform);

	// Now do the actual rotating - a pixel at a time
	BitBlt(destDC, 0, 0, w, h, sourceDC, 0, 0, SRCCOPY);
	// Restore DCs
	::SelectObject(sourceDC, hbmOldSource);
	::SelectObject(destDC, hbmOldDest);

	BITMAP btm;
	::GetObject(hbmResult, sizeof(mImageBM), &mImageBM);

}

istream & operator >> (istream & in, Sprite* sprite) {
	in >> sprite->mPosition.x >> sprite->mPosition.y >> sprite->angle;
	sprite->computeVelocity();
	sprite->computeCenter();
	sprite->Rotate(sprite->angle);
	return in;
}