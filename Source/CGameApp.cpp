//-----------------------------------------------------------------------------
// File: CGameApp.cpp
//
// Desc: Game Application class, this is the central hub for all app processing
//
// Original design by Adam Hoult & Gary Simmons. Modified by Mihai Popescu.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// CGameApp Specific Includes
//-----------------------------------------------------------------------------
#include "CGameApp.h"

extern HINSTANCE g_hInst;
//extern int		 IDCONTINUEGAME;
using namespace std;
//-----------------------------------------------------------------------------
// CGameApp Member Functions
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Name : CGameApp () (Constructor)
// Desc : CGameApp Class Constructor
//-----------------------------------------------------------------------------
CGameApp::CGameApp()
{
	// Reset / Clear all required values
	m_hWnd			= NULL;
	m_hIcon			= NULL;
	m_hMenu			= NULL;
	m_pBBuffer		= NULL;
	//m_pPlayer		= NULL;
	//m_pOponentPlayer = NULL;
	//m_pEnemies		= NULL;
	m_LastFrameRate = 0;
}

//-----------------------------------------------------------------------------
// Name : ~CGameApp () (Destructor)
// Desc : CGameApp Class Destructor
//-----------------------------------------------------------------------------
CGameApp::~CGameApp()
{
	// Shut the engine down
	ShutDown();
}

//-----------------------------------------------------------------------------
// Name : InitInstance ()
// Desc : Initialises the entire Engine here.
//-----------------------------------------------------------------------------
bool CGameApp::InitInstance( LPCTSTR lpCmdLine, int iCmdShow )
{

	// Create the primary display device
	if (!CreateDisplay()) { ShutDown(); return false; }
	SelectGameMode();
	// Build Objects
	if (!BuildObjects()) 
	{ 
		MessageBox( 0, _T("Failed to initialize properly. Reinstalling the application may solve this problem.\nIf the problem persists, please contact technical support."), _T("Fatal Error"), MB_OK | MB_ICONSTOP);
		ShutDown(); 
		return false; 
	}

	// Set up all required game states
	//SetupGameState();

	// Success!
	return true;
}

//-----------------------------------------------------------------------------
// Name : CreateDisplay ()
// Desc : Create the display windows, devices etc, ready for rendering.
//-----------------------------------------------------------------------------
bool CGameApp::CreateDisplay()
{
	LPTSTR			WindowTitle		= _T("GameFramework");
	LPCSTR			WindowClass		= _T("GameFramework_Class");
	USHORT			Width			= 1366;
	USHORT			Height			= 730;
	RECT			rc;
	WNDCLASSEX		wcex			= {0};

	
	wcex.cbSize			= sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= CGameApp::StaticWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= g_hInst;
	wcex.hIcon			= LoadIcon(g_hInst, MAKEINTRESOURCE(IDI_ICON));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= 0;
	wcex.lpszClassName	= WindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON));

	if(atom == 0) {
		atom = RegisterClassEx(&wcex);
	}
	
	if(atom == 0)
		return false;

	// Retrieve the final client size of the window
	::GetClientRect( m_hWnd, &rc );
	m_nViewX		= rc.left;
	m_nViewY		= rc.top;
	m_nViewWidth	= rc.right - rc.left;
	m_nViewHeight	= rc.bottom - rc.top;
	
	
	m_hWnd = CreateWindow(WindowClass, WindowTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, Width, Height, NULL, NULL, g_hInst, this);


	SetWindowPos(m_hWnd, HWND_TOP, 0, 0, Width, Height, SWP_FRAMECHANGED);
	//SetWindowPos(m_hWnd, HWND_TOP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_FRAMECHANGED);
	//PostMessage(m_hWnd, WM_SYSCOMMAND, SC_RESTORE, 0);
	if (!m_hWnd)
		return false;


	// Show the window
	// aici mazimizeaza SW_NORMAL
	ShowWindow(m_hWnd, SW_MAXIMIZE);

	// Success!!
	return true;
}

//-----------------------------------------------------------------------------
// Name : BeginGame ()
// Desc : Signals the beginning of the physical post-initialisation stage.
//		From here on, the game engine has control over processing.
//-----------------------------------------------------------------------------

void CGameApp::SelectGameMode() {
	string output_message = "Select mode\nSingle player = YES\nMultiplayer = NO";
	int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("Game Over"), MB_YESNO | MB_ICONQUESTION);
	if (option == IDYES) {
		SINGLE_PLAYER = 1;
	}
	else  if (option == IDNO) {
		SINGLE_PLAYER = 0;
	}
}

int CGameApp::BeginGame()
{
	MSG		msg;
	int		restart = IDCONTINUEGAME;
	// Start main loop
	/*string output_message = "Select mode\nSingle player = YES\nMultiplayer = NO";
	int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("Game Over"), MB_YESNO | MB_ICONQUESTION);
	if(option == IDYES) {
		SINGLE_PLAYER = 1;
	} else  if(option == IDNO) {
		SINGLE_PLAYER = 0;
		m_pPlayers.push_back(new CPlayer(m_pBBuffer, CPLAYER_INPUT_FILE.c_str()));
		m_pPlayers[1]->Position() = Vec2(300, 100);
		m_pPlayers[0]->SetLivesSide(CPlayer::LEFT_SIDE);
		m_pPlayers[1]->SetLivesSide(CPlayer::RIGHT_SIDE);
	}*/
	while(restart == IDCONTINUEGAME)
	{
		// Did we recieve a message, or are we idling ?
		if ( PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) ) 
		{
			if (msg.message == WM_QUIT) break;
			TranslateMessage( &msg );
			DispatchMessage ( &msg );
		} 
		else 
		{
			// Advance Game Frame.
			restart = FrameAdvance();

		} // End If messages waiting
	
	} // Until quit message is receieved

	return restart;
}

//-----------------------------------------------------------------------------
// Name : ShutDown ()
// Desc : Shuts down the game engine, and frees up all resources.
//-----------------------------------------------------------------------------
bool CGameApp::ShutDown()
{
	// Release any previously built objects
	ReleaseObjects ( );
	
	// Destroy menu, it may not be attached
	if ( m_hMenu ) DestroyMenu( m_hMenu );
	m_hMenu		 = NULL;

	// Destroy the render window
	SetMenu( m_hWnd, NULL );
	if ( m_hWnd ) DestroyWindow( m_hWnd );
	m_hWnd		  = NULL;
	
	// Shutdown Success
	return true;
}

//-----------------------------------------------------------------------------
// Name : StaticWndProc () (Static Callback)
// Desc : This is the main messge pump for ALL display devices, it captures
//		the appropriate messages, and routes them through to the application
//		class for which it was intended, therefore giving full class access.
// Note : It is VITALLY important that you should pass your 'this' pointer to
//		the lpParam parameter of the CreateWindow function if you wish to be
//		able to pass messages back to that app object.
//-----------------------------------------------------------------------------
LRESULT CALLBACK CGameApp::StaticWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	// If this is a create message, trap the 'this' pointer passed in and store it within the window.
	if ( Message == WM_CREATE ) SetWindowLong( hWnd, GWL_USERDATA, (LONG)((CREATESTRUCT FAR *)lParam)->lpCreateParams);

	// Obtain the correct destination for this message
	CGameApp *Destination = (CGameApp*)GetWindowLong( hWnd, GWL_USERDATA );
	
	// If the hWnd has a related class, pass it through
	if (Destination) return Destination->DisplayWndProc( hWnd, Message, wParam, lParam );
	
	// No destination found, defer to system...
	return DefWindowProc( hWnd, Message, wParam, lParam );
}

//-----------------------------------------------------------------------------
// Name : DisplayWndProc ()
// Desc : The display devices internal WndProc function. All messages being
//		passed to this function are relative to the window it owns.
//-----------------------------------------------------------------------------
LRESULT CGameApp::DisplayWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam )
{
	static UINT			fTimer;	

	// Determine message type
	switch (Message)
	{
		case WM_CREATE:
			break;
		
		case WM_CLOSE:
			PostQuitMessage(0);
			break;
		
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		case WM_SIZE:
			if ( wParam == SIZE_MAXIMIZED)
			{
				// App is inactive
				m_bActive = true;

				m_nViewWidth = LOWORD(lParam);
				m_nViewHeight = HIWORD(lParam);
			} // App has been minimized
			else
			{
				// App is active
				m_bActive = true;

				// Store new viewport sizes
				m_nViewWidth  = LOWORD( lParam );
				m_nViewHeight = HIWORD( lParam );


			
			} // End if !Minimized

			break;

		case WM_LBUTTONDOWN:
			// Capture the mouse
			SetCapture( m_hWnd );
			GetCursorPos( &m_OldCursorPos );
			break;

		case WM_LBUTTONUP:
			// Release the mouse
			ReleaseCapture( );
			break;

		case WM_KEYDOWN:
			switch(wParam)
			{
			case VK_ESCAPE:
				PostQuitMessage(0);
				break;
			case VK_RETURN:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				m_pPlayers[0]->Explode();
				break;
			/*case 0x51:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				m_pPlayers[0]->Explode();
				break;*/
			case 0x4E:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				m_pPlayers[0]->RotateLeft();
				break;
			case 0x4D:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				m_pPlayers[0]->RotateRight();
				break;
			case 0x55:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				Save();
				break;
			case 0x49:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				Load();
				break;
			case VK_NUMPAD4:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				try {
					m_pPlayers[1]->RotateLeft();
				} catch(int e) {}
				break;
			case VK_NUMPAD6:
				fTimer = SetTimer(m_hWnd, 1, 250, NULL);
				try {
					m_pPlayers[1]->RotateRight();
				} catch (int e) {}
				break;
			}
			break;	

		case WM_TIMER:
			switch(wParam)
			{
			case 1:
				for(int i = 0; i < m_pPlayers.size(); i++) {
					if(!m_pPlayers[i]->AdvanceExplosion())
						KillTimer(m_hWnd, 1);
				}

				for (int i = 0; i < m_pEnemies.size(); i++) {
					if (!m_pEnemies[i]->AdvanceExplosion())
						KillTimer(m_hWnd, 1);
				}
			}
			break;

		case WM_COMMAND:
			break;

		default:
			return DefWindowProc(hWnd, Message, wParam, lParam);

	} // End Message Switch
	
	return 0;
}

//-----------------------------------------------------------------------------
// Name : BuildObjects ()
// Desc : Build our demonstration meshes, and the objects that instance them
//-----------------------------------------------------------------------------
bool CGameApp::BuildObjects()
{
	m_pBBuffer = new BackBuffer(m_hWnd, m_nViewWidth, m_nViewHeight);
	m_pPlayers.push_back(new CPlayer(m_pBBuffer, CPLAYER_INPUT_FILE.c_str()));
	
	m_pPlayers[0]->Position() = Vec2(300, 600);

	if(SINGLE_PLAYER) {
		m_pEnemies.push_back(new Enemy(m_pBBuffer, ENEMY_INPUT_FILE.c_str()));
		m_pEnemies[0]->Position() = Vec2(300, 100);
	}
	else {
		m_pPlayers.push_back(new CPlayer(m_pBBuffer, CPLAYER_INPUT_FILE.c_str()));
		m_pPlayers[1]->Position() = Vec2(300, 100);
		m_pPlayers[0]->SetLivesSide(CPlayer::LEFT_SIDE);
		m_pPlayers[1]->SetLivesSide(CPlayer::RIGHT_SIDE);
		for (int i = 0; i < 180 / ANGLE_ROTATION; i++)
			m_pPlayers[1]->RotateLeft();

	}

	//m_pBBuffer = new BackBuffer(m_hWnd, m_nViewWidth, m_nViewHeight);
	//m_pPlayer = new CPlayer(m_pBBuffer, CPLAYER_INPUT_FILE.c_str());
	//m_pEnemies.push_back(new Enemy(m_pBBuffer, ENEMY_INPUT_FILE.c_str()));
	////m_pPlayer->SetLivesSide(CPlayer::RIGHT_SIDE);
	if(!m_imgBackground.LoadBitmapFromFile(BACKGROUND_FILE.c_str(), GetDC(m_hWnd)))
		return false;
	//
	//// Success!
	return true;
}

//bool CGameApp::BuildObjectsSinglePlayer()
//{
//
//
//
//	m_pEnemies.push_back(new Enemy(m_pBBuffer, ENEMY_INPUT_FILE.c_str()));
//	//m_pPlayer->SetLivesSide(CPlayer::RIGHT_SIDE);
//	m_pPlayer->Position() = Vec2(300, 600);
//	m_pEnemies[0]->Position() = Vec2(300, 100);
//	if (!m_imgBackground.LoadBitmapFromFile(BACKGROUND_FILE.c_str(), GetDC(m_hWnd)))
//		return false;
//
//	// Success!
//	return true;
//}
//
//
//bool CGameApp::BuildObjectsMultiPlayer()
//{
//
//
//	m_pBBuffer = new BackBuffer(m_hWnd, m_nViewWidth, m_nViewHeight);
//	m_pPlayer = new CPlayer(m_pBBuffer, CPLAYER_INPUT_FILE.c_str());
//	m_pOponentPlayer = new CPlayer(m_pBBuffer, CPLAYER_INPUT_FILE.c_str());
//	m_pPlayer->SetLivesSide(CPlayer::RIGHT_SIDE);
//	m_pOponentPlayer->SetLivesSide(CPlayer::LEFT_SIDE);
//	m_pPlayer->Position() = Vec2(300, 600);
//	m_pOponentPlayer->Position() = Vec2(300, 100);
//
//	if (!m_imgBackground.LoadBitmapFromFile(BACKGROUND_FILE.c_str(), GetDC(m_hWnd)))
//		return false;
//
//	// Success!
//	return true;
//}

//-----------------------------------------------------------------------------
// Name : SetupGameState ()
// Desc : Sets up all the initial states required by the game.
//-----------------------------------------------------------------------------
void CGameApp::SetupGameState()
{
	m_pPlayers[0]->Position() = Vec2(300, 600);
	m_pEnemies[0]->Position() = Vec2(300, 100);
}

//-----------------------------------------------------------------------------
// Name : ReleaseObjects ()
// Desc : Releases our objects and their associated memory so that we can
//		rebuild them, if required, during our applications life-time.
//-----------------------------------------------------------------------------
void CGameApp::ReleaseObjects( )
{
	/*if(m_pPlayer != NULL)
	{
		delete m_pPlayer;
		m_pPlayer = NULL;
	}
	if (m_pOponentPlayer != NULL)
	{
		delete m_pOponentPlayer;
		m_pOponentPlayer = NULL;
	}*/

	if (m_pPlayers.size() != 0)
	{
		for (int i = 0; i < m_pPlayers.size(); i++) {
			delete m_pPlayers[i];
		}
		m_pPlayers.clear();
	}

	if(m_pBBuffer != NULL)
	{
		delete m_pBBuffer;
		m_pBBuffer = NULL;
	}

	if (m_pEnemies.size() != 0)
	{
		for(int i = 0; i < m_pEnemies.size(); i++) {
			delete m_pEnemies[i];
		}
		m_pEnemies.clear();
	}

}

//-----------------------------------------------------------------------------
// Name : FrameAdvance () (Private)
// Desc : Called to signal that we are now rendering the next frame.
//-----------------------------------------------------------------------------
int CGameApp::FrameAdvance()
{
	static TCHAR FrameRate[ 50 ];
	static TCHAR TitleBuffer[ 255 ];

	// Advance the timer
	m_Timer.Tick( );

	// Skip if app is inactive
	if ( !m_bActive ) return IDCONTINUEGAME;
	
	// Get / Display the framerate
	if ( m_LastFrameRate != m_Timer.GetFrameRate() )
	{
		m_LastFrameRate = m_Timer.GetFrameRate( FrameRate, 50 );
		sprintf_s( TitleBuffer, _T("Game : %s"), FrameRate );
		SetWindowText( m_hWnd, TitleBuffer );

	} // End if Frame Rate Altered

	// Poll & Process input devices
	ProcessInput();

	// Animate the game objects
	AnimateObjects();
	CheckCollisions();
	// Drawing the game objects
	DrawObjects();
	srand(time(NULL));
	//int new_enemy = rand() % NEW_ENEMY_CHANCHE;
	//if (new_enemy == 1) {
	//	m_pEnemies.push_back(new Enemy(m_pBBuffer, "data\\bullet.bmp"));
	//}
	
	for(int i = 0; i < m_pPlayers.size(); i++) {
		if(m_pPlayers[i]->get_lives() == 0) {
			cout << "game over";
			//string output_message = "Your score is : " + to_string(m_pPlayers[i]->getScore()) + "\nDo you want to play again?";
			string output_message = "Player " + to_string(i) + " won!\nDo you want to play again?";
			int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("Game Over"), MB_YESNO | MB_ICONQUESTION);
			if (option == IDNO) {
				ShutDown();
			}
			else if (option == IDYES) {
				cout << "restart";
			}
			return option;
			//ShutDown();
		}
	}

	//if(m_pPlayers[0]->get_lives() == 0 ) {
	//	cout << "game over";
	//	string output_message = "Your score is : " + to_string(m_pPlayer->getScore()) + "\nDo you want to play again?";
	//	int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("Game Over"), MB_YESNO | MB_ICONQUESTION);
	//	if(option == IDNO) {
	//		ShutDown();
	//	} else if(option == IDYES) {
	//		cout << "restart";
	//	}
	//	return option;
	//	//ShutDown();
	//}
	//if (m_pOponentPlayer->get_lives() == 0) {
	//	cout << "game over";
	//	string output_message = "Your score is : " + to_string(m_pPlayer->getScore()) + "\nDo you want to play again?";
	//	int option = MessageBox(m_hWnd, _T(output_message.c_str()), _T("Game Over"), MB_YESNO | MB_ICONQUESTION);
	//	if (option == IDNO) {
	//		ShutDown();
	//	}
	//	else if (option == IDYES) {
	//		cout << "restart";
	//	}
	//	return option;
	//	//ShutDown();
	//}
	return IDCONTINUEGAME;
}

//-----------------------------------------------------------------------------
// Name : ProcessInput () (Private)
// Desc : Simply polls the input devices and performs basic input operations
//-----------------------------------------------------------------------------
void CGameApp::ProcessInput()
{
	static UCHAR pKeyBuffer[256];
	ULONG		Direction[2] = { 0, 0 };
	POINT		CursorPos;
	float		X = 0.0f, Y = 0.0f;

	// Retrieve keyboard state
	if (!GetKeyboardState(pKeyBuffer)) return;

	// Check the relevant keys
	if (pKeyBuffer[0x57] & 0xF0) Direction[0] |= CPlayer::DIR_FORWARD;
	if (pKeyBuffer[0x53] & 0xF0) Direction[0] |= CPlayer::DIR_BACKWARD;
	if (pKeyBuffer[0x41] & 0xF0) Direction[0] |= CPlayer::DIR_LEFT;
	if (pKeyBuffer[0x44] & 0xF0) Direction[0] |= CPlayer::DIR_RIGHT;
	if (pKeyBuffer[VK_SPACE] & 0xF0) m_pPlayers[0]->Shoot();

	if (pKeyBuffer[VK_UP] & 0xF0) Direction[1] |= CPlayer::DIR_FORWARD;
	if (pKeyBuffer[VK_DOWN] & 0xF0) Direction[1] |= CPlayer::DIR_BACKWARD;
	if (pKeyBuffer[VK_LEFT] & 0xF0) Direction[1] |= CPlayer::DIR_LEFT;
	if (pKeyBuffer[VK_RIGHT] & 0xF0) Direction[1] |= CPlayer::DIR_RIGHT;
	if (pKeyBuffer[VK_NUMPAD5] & 0xF0) m_pPlayers[1]->Shoot();

	// Move the player
	for (int i = 0; i < m_pPlayers.size(); i++) {
		m_pPlayers[i]->Move(Direction[i]);
	}

	for (int i = 0; i < m_pEnemies.size(); i++) {
		m_pEnemies[i]->Move();
	}

	

	// Now process the mouse (if the button is pressed)
	if ( GetCapture() == m_hWnd )
	{
		// Hide the mouse pointer
		SetCursor( NULL );

		// Retrieve the cursor position
		GetCursorPos( &CursorPos );

		// Reset our cursor position so we can keep going forever :)
		SetCursorPos( m_OldCursorPos.x, m_OldCursorPos.y );

	} // End if Captured
}

//-----------------------------------------------------------------------------
// Name : AnimateObjects () (Private)
// Desc : Animates the objects we currently have loaded.
//-----------------------------------------------------------------------------
void CGameApp::AnimateObjects()
{
	//if(new_enemy == 0) {
	//	new_enemy = rand() % NEW_ENEMY_CHANCHE + NEW_ENEMY_CHANCHE;
	//	m_pEnemies.push_back(new Enemy(m_pBBuffer, ENEMY_INPUT_FILE.c_str()));
	//}
	//new_enemy--;
	for (int i = 0; i < m_pPlayers.size(); i++) {
		m_pPlayers[i]->Update(m_Timer.GetTimeElapsed());
	}
	
	for (int i = 0; i < m_pEnemies.size(); i++) {
		m_pEnemies[i]->Update(m_Timer.GetTimeElapsed());
	}
		
}

//void CGameApp::CheckCollisionsSinglePlayer() {
//	for(int i = 0; i < m_pEnemies.size(); i++) {
//		if (m_pEnemies[i]->checkCollision(m_pPlayer->getBullets())) {
//			m_pPlayer->hittedSomething();
//
//			SetTimer(m_hWnd, 1, 250, NULL);
//			m_pEnemies[i]->Explode();
//			m_pPlayer->deleteBullets();
//			m_pEnemies[i]->deleteBullets();
//			//m_pEnemies.erase(m_pEnemies.begin() + i);
//		}
//
//		if (m_pPlayer->checkCollision(m_pEnemies[i]->getBullets())) {
//			SetTimer(m_hWnd, 1, 250, NULL);
//			m_pPlayer->wasHitted();
//			m_pPlayer->Explode();
//			m_pPlayer->deleteBullets();
//			m_pEnemies[i]->deleteBullets();
//		}
//		if (m_pEnemies[i] == NULL)
//			m_pEnemies.erase(m_pEnemies.begin() + i);
//	}
//	
//}
//
//void CGameApp::CheckCollisionsMultiPlayer() {
//	if (m_pPlayer->checkCollision(m_pOponentPlayer->getBullets())) {
//		SetTimer(m_hWnd, 1, 250, NULL);
//		m_pPlayer->wasHitted();
//		m_pPlayer->Explode();
//		m_pPlayer->deleteBullets();
//		m_pOponentPlayer->deleteBullets();
//	}
//	if (m_pOponentPlayer->checkCollision(m_pPlayer->getBullets())) {
//		SetTimer(m_hWnd, 1, 250, NULL);
//		m_pOponentPlayer->wasHitted();
//		m_pOponentPlayer->Explode();
//		m_pOponentPlayer->deleteBullets();
//		m_pPlayer->deleteBullets();
//	}
//}

void CGameApp::CheckCollisions() {
	for(int i = 0; i < m_pPlayers.size(); i++) {
		for(int j = 0; j < m_pPlayers.size(); j++) {
			if(i != j) {
				if (m_pPlayers[i]->checkCollision(m_pPlayers[j]->getBullets())) {
					SetTimer(m_hWnd, 1, 250, NULL);
					m_pPlayers[i]->wasHitted();
					m_pPlayers[i]->Explode();
					m_pPlayers[i]->deleteBullets();
					m_pPlayers[j]->deleteBullets();
				}
			}
		}
		for(int j = 0; j < m_pEnemies.size(); j++) {
			if (m_pPlayers[i]->checkCollision(m_pEnemies[j]->getBullets())) {
				SetTimer(m_hWnd, 1, 250, NULL);
				m_pPlayers[i]->wasHitted();
				m_pPlayers[i]->Explode();
				m_pPlayers[i]->deleteBullets();
				m_pEnemies[j]->deleteBullets();
			}
		}
	}

			/*}
	if (SINGLE_PLAYER)
		CheckCollisionsSinglePlayer();
	else
		CheckCollisionsMultiPlayer();*/
}

//-----------------------------------------------------------------------------
// Name : DrawObjects () (Private)
// Desc : Draws the game objects
//-----------------------------------------------------------------------------
void CGameApp::DrawObjects()
{
	m_pBBuffer->reset();
	if (-scrolling_backgroung >= m_imgBackground.Height() - m_pBBuffer->height())
		scrolling_backgroung = 0;
	scrolling_backgroung -= SCROLLING_SPEED;

	m_imgBackground.Paint(m_pBBuffer->getDC(), 0, scrolling_backgroung);

	for(int i = 0; i < m_pPlayers.size(); i++) {
		m_pPlayers[i]->Draw();
		m_pPlayers[i]->DrawLives();
		m_pPlayers[i]->DrawScore();
	}
	for (int i = 0; i < m_pEnemies.size(); i++) {
		m_pEnemies[i]->Draw();
	}

	m_pBBuffer->present();
}


int CGameApp::RestartGame(LPCTSTR lpCmdLine, int iCmdShow) {

	ReleaseObjects();
	if (!BuildObjects()) return 0;
	SetupGameState();
	int retCode = BeginGame();
	
	return retCode;
}

void CGameApp::Save() {
	ofstream file;
	file.open(save_file);
	file << SINGLE_PLAYER << "\n";
	file << m_pPlayers.size() << "\n";
	for(int i = 0; i < m_pPlayers.size(); i++) {
		file << m_pPlayers[i]->toString();
	}
	file << to_string(m_pEnemies.size()) << "\n";
	for (int i = 0; i < m_pEnemies.size(); i++) {
		file << m_pEnemies[i]->toString();
	}
	file.close();
//}
	//if (SINGLE_PLAYER)
	//	SaveSinglePlayer();
	//else
	//	SaveMultiPlayer();
}
	/*
void CGameApp::SaveSinglePlayer() {
	ofstream file;
	file.open(save_file);
	file << SINGLE_PLAYER << "\n";
	file << m_pPlayer->toString();
	file << to_string(m_pEnemies.size()) << "\n";
	for (int i = 0; i < m_pEnemies.size(); i++) {
		file << m_pEnemies[i]->toString();
	}
	file.close();
}

void CGameApp::SaveMultiPlayer() {
	ofstream file;
	file.open(save_file);
	file << SINGLE_PLAYER << "\n";
	file << m_pPlayer->toString();
	file << m_pOponentPlayer->toString();
	file.close();
}*/

//void CGameApp::LoadSinglePlayer() {
//	ReleaseObjects();
//	int no_enemies;
//	int no_players;
//	if (!BuildObjects()) return;
//
//	ifstream file;
//	file.open(save_file);
//	file >> no_players;
//	m_pPlayers.resize(no_players);
//	for (int i = 0; i < no_players; i++) {
//		file >> m_pPlayers[i];
//	}
//	file >> no_enemies;
//	m_pEnemies.resize(no_enemies);
//	for (int i = 0; i < no_enemies; i++) {
//		file >> m_pEnemies[i];
//	}
//	file.close();
//}

//void CGameApp::LoadMultiPlayer() {
//	ReleaseObjects();
//	//int no_enemies;
//	if (!BuildObjects()) return;
//
//	ifstream file;
//	file.open(save_file);
//
//	file >> m_pPlayer;
//	file >> m_pOponentPlayer;
//
//	file.close();
//}

void CGameApp::Load() {

	ReleaseObjects();
	int no_enemies;
	int no_players;


	ifstream file;
	file.open(save_file);
	file >> SINGLE_PLAYER;

	if (!BuildObjects()) return;

	file >> no_players;
	m_pPlayers.resize(no_players);
	for (int i = 0; i < no_players; i++) {
		file >> m_pPlayers[i];
	}
	file >> no_enemies;
	m_pEnemies.resize(no_enemies);
	for (int i = 0; i < no_enemies; i++) {
		file >> m_pEnemies[i];
	}
	file.close();
}
